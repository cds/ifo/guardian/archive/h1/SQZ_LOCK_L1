# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# functions for writing, switching, etc. quickly
# by Matthew Evans and Jenne Driggers (July 2015)

import threading
from time import sleep
import ezca as ez

##################################################
# user functions:
#
# write_many - simultaneous writes
# switch_many - simultaneous filter switching
# do_many - simultaneous groups of writes and switches
#
##################################################

# this function executes multiple ezca writes simultaneously
#   the first argument is an ezca.Ezca instance (must import ezca)
#   argList is a list of tuples, where the tuples are arguements to ezca.write
#
# Example:
# import ezca as ez
# import fast_ezca as fez
#
# ezca = ez.Ezca()
# argList = [('LSC-LOCKIN_1_MTRX_2_1', '0.0'), ('LSC-LOCKIN_1_MTRX_2_2', '0.0'), \
#            ('LSC-LOCKIN_1_MTRX_2_3', '0.0'), ('LSC-LOCKIN_1_MTRX_2_4', '0.0')]
# fez.write_many(ezca, writeList)
#
# see test_write_many.py for an extensive example

def write_many(ezca, argList):

  # start a thread for each tuple of arguments
  threads = []
  for args in argList:
    # launch threads
    t = threading.Thread(target=ezca.write, args=args)
    threads.append(t)
    t.start()
    
    # don't go too fast or Epics will have trouble
    #sleep(0.005)
    sleep(0.02)

  # wait for the threads to finish
  for t in threads:
    t.join()


# this function executes multiple ezca switch commands simultaneously
#   the first argument is an ezca.Ezca instance (must import ezca)
#   argList is a list of tuples, where the tuples are arguements to ezca.switch
#
# Example:
# switchList = [('LSC-LOCKIN_1_DEMOD_2_SIG', 'INPUT', 'ON', 'OFFSET', 'OFF'), \
#   ('LSC-LOCKIN_1_DEMOD_2_I', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_2_Q', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_3_I', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_3_Q', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_4_I', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_4_Q', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_5_I', 'INPUT', 'ON', 'FM6', 'ON'), \
#   ('LSC-LOCKIN_1_DEMOD_5_Q', 'INPUT', 'ON', 'FM6', 'ON')]
# 
# fez.switch_many(ezca, switchList)

def switch_many(ezca, argList):

  # start a thread for each tuple of arguments
  threads = []
  for args in argList:
    # launch threads
    t = threading.Thread(target=run_switch_many, args=(ezca, args))  
    threads.append(t)
    t.start()
    
    # don't go too fast or Epics will have trouble
    #sleep(0.01)
    sleep(0.03)

  # wait for the threads to finish
  for t in threads:
    t.join()


# this function waits for multiple filter modules to finish ramping and/or engaging filters
#   the first argument is an ezca.Ezca instance (must import ezca)
#   filterList is a list of filter names or LIGOfilter objects
#
# Example:
# filterList = ['LSC-LOCKIN_1_DEMOD_2_SIG', \
#   'LSC-LOCKIN_1_DEMOD_2_I', 'LSC-LOCKIN_1_DEMOD_2_Q', \
#   'LSC-LOCKIN_1_DEMOD_3_I', 'LSC-LOCKIN_1_DEMOD_3_Q']
# 
# fez.switch_many(ezca, filterList)

def wait_many(ezca, filterList):

  # start a thread for each tuple of arguments
  threads = []
  for filt in filterList:
    # convert filter to name
    if isinstance(filt, ez.LIGOFilter):
      filt = filt.filter_name

    # launch threads
    t = threading.Thread(target=run_wait_many, args=(ezca, filt))  
    threads.append(t)
    t.start()
    
    # don't go too fast or Epics will have trouble
    sleep(0.01)

  # wait for the threads to finish
  for t in threads:
    t.join()

# this function executes multiple ezca switch and/or switch commands simultaneously
#   the first argument is an ezca.Ezca instance (must import ezca)
#   actionList is a list of tuples, where the tuples are arguements to ezca.write or ezca.switch, or
#     various filter operations. The first element in the tuple specifies the type of action,
#     while the other elements are arguments for the speficied function.  Valid types of actions are:
#       write, wr, w    = write a value with ezca.write
#       switch, sw, s   = switch a filter with ezca.switch
#       ramp_gain, rg   = ramp a filter gain (arguments are filter name, new value and ramp time)
#       ramp_offset, ro = ramp a filter offset (arguments are filter name, new value and ramp time)
#       turn_on, on     = as switch, but all specified filter bits are switched ON
#       turn_off, off   = as switch, but all specified filter bits are switched OFF
#       clear           = clear the history of a filter module (RSET = 2)
#
#   Alternatively, an element of the actionList may itself be a list of tuples.  In this case
#   the elements of the list are excuted sequentially.  In this way do_many can be used to
#   simultaneously execute many small sequences of commands.
#
#   All filter names can be replace with LIGOFilter objects.  ramp_gain and ramp_offset do not wait
#   for the ramp to finish by default, but if a LIGOFilter object is used all arguments will be
#   passed to the corresponding LIGOFilter ramp function, so an additional argument of True will
#   cause the execution to block until the ramp is finished.
#
#
#
# Example of multiple actions, with some groups of actions:
#
# doList = [('s',  'LSC-LOCKIN_1_DEMOD_2_SIG', 'INPUT', 'ON', 'OFFSET', 'OFF'), \
#   [('w', 'LSC-LOCKIN_1_MTRX_2_1', '0.0'), \
#    ('s', 'LSC-LOCKIN_1_DEMOD_2_I', 'INPUT', 'ON', 'FM6', 'ON'), \
#    ('s', 'LSC-LOCKIN_1_DEMOD_2_Q', 'INPUT', 'ON', 'FM6', 'ON')], \
#   [('w', 'LSC-LOCKIN_1_MTRX_3_1', '0.0'), \
#    ('s', 'LSC-LOCKIN_1_DEMOD_3_I', 'INPUT', 'ON', 'FM6', 'ON'), \
#    ('s', 'LSC-LOCKIN_1_DEMOD_3_Q', 'INPUT', 'ON', 'FM6', 'ON')], \
#   ('w', 'LSC-LOCKIN_1_MTRX_2_2', '0.0'), ('w', 'LSC-LOCKIN_1_MTRX_3_2', '0.0'), \
#   ('s', 'LSC-LOCKIN_1_DEMOD_4_I', 'INPUT', 'OFF', 'FM6', 'ON'), \
#   ('s', 'LSC-LOCKIN_1_DEMOD_4_Q', 'INPUT', 'OFF', 'FM6', 'ON'), \
#   ('s', 'LSC-LOCKIN_1_DEMOD_5_I', 'INPUT', 'OFF', 'FM6', 'ON'), \
#   ('s', 'LSC-LOCKIN_1_DEMOD_5_Q', 'INPUT', 'OFF', 'FM6', 'ON')]
# fez.do_many(ezca, doList)
#
# Example with other types of actions:
# (note: do not ramp gain and offset simultaneously, since they will fight over the ramp time)
#
# doList = [('off', 'LSC-LOCKIN_1_DEMOD_4_I', 'FM6', 'OUTPUT', 'INPUT'), \
#           ('on', 'LSC-LOCKIN_1_DEMOD_4_Q', 'FM6', 'OUTPUT', 'INPUT'), \
#           [('rg', 'LSC-LOCKIN_1_DEMOD_4_Q', 0, 3), ('ro', 'LSC-LOCKIN_1_DEMOD_4_Q', 0, 1)]]
# fez.do_many(ezca, doList)

def do_many(ezca, doList):
  # start a thread for each action in the list
  threads = []
  for action in doList:
    # launch threads
    t = threading.Thread(target=run_do_many, args=(ezca, action))
    threads.append(t)
    t.start()
    
    # don't go too fast or Epics will have trouble
    #sleep(0.01)
    sleep(0.04)

  # wait for the threads to finish
  for t in threads:
    t.join()

##################################################
# backend functions
##################################################

# thread entry point for switch_many
def run_switch_many(ezca, args):
  if isinstance(args[0], ez.LIGOFilter):
    args[0].switch(*args[1:], wait=False)
  else:
    ezca.switch(*args)

# thread entry point for switch_many
def run_wait_many(ezca, name):

  # masks
  gain_ramp_bit = 4096      # 2**12
  offset_ramp_bit = 8192    # 2**13

  filt_sw1_mask = 21840     # sum(2**[4:2:14])
  filt_sw2_mask = 85        # sum(2**[0:2:6])

  max_filter_wait = 100     # maximum number of wait cycles for filter engage

  # loop until not ramping (break at end)
  #  wait time limited to prevent hanging the guardian forever
  n_filter_wait = 0
  while True:

    # read binary switch values
    sw1r = int(ezca.read(name + '_SW1R'))
    sw2r = int(ezca.read(name + '_SW2R'))


    # check to see if gain or offset is ramping
    is_gain_ramping = bool(gain_ramp_bit & sw2r)
    is_offset_ramping = bool(offset_ramp_bit & sw2r)

    # mask filter bits for request bit, and engaged bits
    sw1_req = sw1r & filt_sw1_mask
    sw2_req = sw2r & filt_sw2_mask
    sw1_eng = (sw1r / 2) & filt_sw1_mask  # engaged bits are just above request bits
    sw2_eng = (sw2r / 2) & filt_sw2_mask
  

    # check for filters still engaging
    is_filter_ramping = (sw1_req != sw1_eng) or (sw2_req != sw2_eng)

    # debugging output
    #print '%s : sw1 %i,  sw2 %i'%(name, sw1r, sw2r)
    #print ' req %i %i'%(sw1_req, sw2_req)
    #print ' eng %i %i'%(sw1_eng, sw2_eng)
    #print ' bools %i %i %i'%(is_gain_ramping, is_offset_ramping, is_filter_ramping)

    # if we are waiting for filters to engage, increment the filter wait counter
    if is_filter_ramping:
      n_filter_wait += 1

    # break if nothing is ramping
    if not (is_gain_ramping or is_offset_ramping or (is_filter_ramping and n_filter_wait < max_filter_wait)):
      break

    # otherwise, sleep a bit
    sleep(0.1)


# thread entry point for do_many
def run_do_many(ezca, actionList):
  # if just one action, pack it into a list
  if isinstance(actionList, tuple):
    actionList = [actionList]
  
  # do all requested actions sequantially
  for action in actionList:
    args = action[1:]
    if action[0] == 'write' or action[0] == 'wr' or action[0] == 'w':
      # simple write
      ezca.write(*args)
    elif action[0] == 'switch' or action[0] == 'sw' or action[0] == 's':
      # filter switch
      if isinstance(args[0], ez.LIGOFilter):
        args[0].switch(*args[1:], wait=False)
      else:
        ezca.switch(*args)
    elif action[0] == 'ramp_gain' or action[0] == 'rg':
      # gain ramping
      name = args[0]
      if isinstance(name, ez.LIGOFilter):
        if len(args) == 3:
          # just a name and 2 args, so set wait flag to false
          name.ramp_gain(args[1], args[2], wait=False)
        else:
          # more than 2 args, so let the user deal with wait flag
          name.ramp_gain(*args[1:])
      else:
        ezca.write(name + '_TRAMP', args[2])
        ezca.write(name + '_GAIN', args[1])
    elif action[0] == 'ramp_offset' or action[0] == 'ro':
      # offset ramping
      name = args[0]
      if isinstance(name, ez.LIGOFilter):
        if len(args) == 3:
          name.ramp_offset(args[1], args[2], wait=False)
        else:
          name.ramp_offset(*args[1:])
      else:
        ezca.write(name + '_TRAMP', args[2])
        ezca.write(name + '_OFFSET', args[1])
    elif action[0] == 'turn_on' or action[0] == 'on':
      # switch on
      name = args[0]
      if isinstance(name, str):
        name = ezca.get_LIGOFilter(name)
      name.turn_on(*args[1:], wait=False)
    elif action[0] == 'turn_off' or action[0] == 'off':
      # switch off
      name = args[0]
      if isinstance(name, str):
        name = ezca.get_LIGOFilter(name)
      name.turn_off(*args[1:], wait=False)
    elif action[0] == 'clear' or action[0] == 'clr':
      # clear history
      name = args[0]
      if isinstance(name, ez.LIGOFilter):
        name.RSET.put(2)
      else:
        ezca.write(name + '_RSET', 2)
    else:
      print 'ERROR in do_many: Unknown action \"%s\" requested'%(action[0])
