#
# $Id$
# $HeadURL$

import numpy


# If you change these input powers, you *must* also reload the LASER_PWR guardian
# before doing an initial alignment.
# The LASER_PWR guardian creates fast states based on these numbers, and ALIGN_IFO guardian tries to
# go to those fast states.  So, ALIGN_IFO guardian will fail if the other guardian LASER_PWR
# states don't exist yet.
input_power = {'PRXY': 2,
               'MICH': 10,
               'SRXY': 10,
               'INCREASE_POWER':  20}
# normally MICH=10, SRXY=10, but lowering for time when HAM6 in air

### ISS Final Gain Value
ISS_FinalGain = 12

### CARM Gain sliders initial values for the beginning of locking
# IMC
IMC_FASTGAIN_PowerUp_Scaler = 6 # CC: Value we want on IMC_FASTGAIN when at 2 watts, IMC_FASTGAIN is controlled by ISC_library.IMC_power_adjust()
IMC_IN1_gain = 19 #  CC: Jan 23, 2019, moving 6 dB from IMC IN1/IN2 GAIN to IMC FAST GAIN
IMC_MCL_gain = 1
# COMM
COMM_IMC_IN2GAIN = -12 # CC: Jan 23, 2019, defining new initial COMM gain here
COMM_LSC_IN2GAIN = -32
COMM_LSC_FASTGAIN = 7
COMM_HANDOFF_PART2_LSC_IN2GAIN = 17
# PREP_TR_CARM
LSC_REFLBIAS_GAIN_PREP_TR_CARM = 50.4
LSC_REFL_SUM_A_IN1GAIN_PREP_TR_CARM = 0
LSC_REFL_SERVO_IN1GAIN_PREP_TR_CARM = -16
# START_TR_CARM
ALS_C_REFL_DC_BIAS_GAIN_START_TR_CARM = 24
# CARM_TO_TR
# CARM_150_PICOMETERS
ALS_C_REFL_DC_BIAS_GAIN_CARM_150_PICOMETERS = 39
# CARM_OFFSET_REDUCTION
LSC_REFLBIAS_GAIN_CARM_OFFSET_REDUCTION = 86
# CARM_TO_REFL
LSC_TR_REFLAIR9_CARM_TO_REFL = -0.8

# Defining omc_sign for DARM_OFFSET state
omc_sign = 1

# Modulation depths (in slider units)
mod_depth = {
    '9':  23.4,
    '45': 27.0,
}

gate_valve_flag = False

# Which ESD to use for ALS locking?
ALS_ESD = 'X'

ETMX_ESD_LOWNOISE_BIAS = -1 # -1 means -400 Volts

gain = {'MICH_DARK':
                {'ACQUIRE':4000,
                 'LOCKED': 8000}, # was 2000 for ACQ, 1000 for LOCKED, changed by TVo and Hang
        'MICH_BRIGHT':
                {'ACQUIRE':-4000,
                 'LOCKED': -8000}, #was 2000 for acq and 1000 for locked, changed GLM Sept 13 2018
        'PRXY':-3200,
        'SRXY':10000, # changed from 10000, SED CRC July 22 2018, back to 10000 SB Aug 25 2018
        'XARM_IR':0.04,
        'YARM_IR':0.04,
        'DRMI_MICH':{'ACQUIRE':1.1, # 2018-11-28 TVo, SED from 2.8 as a guess from estimating the PRMI OLTF
                     'NO_ARMS':3.0, # 2018-11-26 TVo changed to 3.0 to match the OLG
                     'W_ALS': 2.8, # was 1.4, increased July 28 2018, was 1.4 - now in FM2
                     '3F_135': 1.115, # 2.23 #changed from 1.25 SED CRC June 21 2018
                     '3F_27I': -0.674, # -1.348
                     '1F_45': 1.824, # multiplied for whitneing gain change alog 44420
                     '1F_9': 0.0}, # removed 2018-10-17 # 0.00643 # 0.009 # added to remove PRCL to MICH coupling GV 2018-08-07
        'DRMI_PRCL':{'ACQUIRE':12, # was 12 # was 16, changed 10Sept2018 JCD
                     'NO_ARMS':18,
                     'W_ALS': 8,  # was 8, changed July 28 2018
                     '3F_27':  -1.22, # was -2.43
                     '1F_9': 0.037},  #0.035
        'DRMI_SRCL':{'ACQUIRE':-30,
                     'NO_ARMS':-33,
                     'W_ALS':  -18,
                     '3F_27':  2.124,
                     '3F_135':  1.7,
                     '1F_9':  0.04, # tuned 190110, soz RXA, # tuned 181224, RXA - stop changing this Stefan !!
                     '1F_45': 1.856}, #mulitppiled by 32 for whitening gain change alog 44420
        'PRMI_MICH':{'W_ALS':3.2,   # Changed from 2.8, measured OLG TVo, SED 20181128
                     'NO_ARMS':2.8}, # changed from 2.8, SED CRC June 24 2018
        'PRMI_PRCL':{'W_ALS':5,       # 12 is too high.  JCD 10Sept2018
                     'NO_ARMS':10, # Changed from 16, measured OLG TVo, SED 20181128
                     'CARRIER':-30},
        'MICHFF':  1,
        'SRCLFF1':  1,
        'SRCLFF2':  0,
        'ALS_ASC_DOF3': {'X': 0.1, 'Y': 0.03},
        }

asc_gains = {'DHARD': {'P':{'DHARD_WFS_initial':-1,
                            'DHARD_WFS_final':-10,
                            'RESONANCE':-30,
                            'ENGAGE_ASC_FOR_FULL_IFO':-50,
                            'LOWNOISE_ASC':-42,              # Was -30, with new 1Hz resG need -42. JCD 6Feb2019
                            },
                        'Y':{'DHARD_WFS_initial':-1,
                            'DHARD_WFS_final':-15,
                            'RESONANCE':-40,
                            'ENGAGE_ASC_FOR_FULL_IFO':-60,
                            'LOWNOISE_ASC':-40,
                            },
                       },#close DHARD
                'CHARD':{'P':{'ENGAGE_ASC_FOR_FULL_IFO':2,
                              'LOWNOISE_ASC':0.6,
                             },
                        'Y':{'ENGAGE_ASC_FOR_FULL_IFO_initial':2.5,
                              'ENGAGE_ASC_FOR_FULL_IFO_final':25,
                             },
                        },#close CHARD
                'CSOFT':{'P':{'ENGAGE_SOFT_LOOPS':15,
                              'LOWNOISE_ASC':20, #alog 46975         
                             },#close csoft p
                          'Y':{'ENGAGE_SOFT_LOOPS_initial':2.5, #CSOFT Y is not used, but there is an if statement and a flag
                               'ENGAGE_SOFT_LOOPS_final':3, #probably unnecessary to make such a small gain adjustment
                              },#close csoft y
                            },#close csoft
                'DSOFT':{'P':{'ENGAGE_SOFT_LOOPS':10, # was 15
                              'LOWNOISE_ASC':10,      # was 30 20190207, double gain to help suppress 0.47Hz osc. JCD 1Feb2019
                             },#close dsoft p
                          'Y':{'ENGAGE_SOFT_LOOPS_initial':2.5, #DSOFT Y is not used, but there is an if statement and a flag
                               'ENGAGE_SOFT_LOOPS_final':4, #probably unnecessary to make such a small gain adjustment
                              },#close dsoft y
                            },#close dsoft
                'MICH':{'P':{'ENGAGE_DRMI_ASC':-0.25,
                            },
                        'Y':{'ENGAGE_DRMI_ASC':-0.2,
                            },
                        },#close MICH
                'RPC':{'DHARD_P':{'14':0,
                              '19':-0.4,
                              '22.5':-0.8,
                              '24.5':-0.9,
                              '27.2':-1,
                              '29.5':-1.1,
                              '33':-1.2,
                              '36.5':-1.45,
                              '40':-1.6,
                            }, #close DHARD RPC P
                          'DHARD_Y':{'14':0,
                              '19':-0.4,
                              '22.5':-0.8,
                              '24.5':-0.9,
                              '27.2':-1,
                              '29.5':-1.1,
                              '33':-1.2,
                              '36.5':-1.45,
                              '40':-1.6,
                            }, #close RPC DHARD Y
                       'CHARD_P':{'14':0,
                                  '19':0.4,
                                  '22.5':0.8,
                                  '24.5':0.9,
                                  '27.2':1,
                                  '29.5':1.1,
                                  '33':1.2,
                                  '36.5':1.45,
                                  '40':1.6,
                                }, #close RPC CHARD P
                         'CHARD_Y':{'14':0,
                                  '19':0.6,
                                  '22.5':1.2,
                                  '24.5':1.3,
                                  '27.2':1.3,
                                  '29.5':1.6,
                                  '33':1.8,
                                  '36.5':2,
                                  '40':2.3,
                                }, #close  CHARD Y RPC
	                       'CSOFT_P':{'14':0,
	                                  '19':0.5,
	                                  '22.5':1,
	                                  '24.5':1.2,
	                                  '27.2':1.3,
	                                  '29.5':1.5,
	                                  '33':1.8,
	                                  '36.5':2.2,
	                                  '40':2.4,
	                                }, #close csoft RPC P
	                       'DSOFT_P':{'14':0,	                                  '19':0.5,
	                                  '22.5':1,
	                                  '24.5':1.2,
	                                  '27.2':1.3,
	                                  '29.5':1.5,
	                                  '33':1.8,
	                                  '36.5':2.2,
	                                  '40':2.4,
	                                }, #close dsoft RPC P
                        },# close RPC
            }#close ASC gains

# filters to use when acquiring
filtmods = {'MICH':   ['FM5'],
            'PRCL':   ['FM4', 'FM9', 'FM10'],
            'SRCL':   ['FM9', 'FM10'],
            'ARM_IR': ['FM3', 'FM4', 'FM6'],
            'PRM_M2': ['FM1', 'FM3', 'FM4', 'FM6', 'FM10'],
            'SRM_M1': ['FM2', 'FM4', 'FM6', 'FM8', 'FM10'],
            }

sus_crossover = {'PRM_M1': -0.02,
                 'PRM_M2': 1,
                 'SRM_M1': -0.02} # changed from -0.02, which produced too much ringing in M1_LF


# DC readout parameters
dc_readout ={
    'DCPD_SUM_TARG': 20,
    'PREF':          1.6868,
    'ERR_GAIN':     -2.1855e-6,
    'x0':            42,
    'sign':          1,
}
# O2 values
#dc_readout ={
#    'DCPD_SUM_TARG': 20,
#    'PREF':          1.3689,
#    'ERR_GAIN':     -8.7614e-7,
#    'x0':            15.233,
#    'sign':          1,
#}


# trigger thresholds  # for several changes for DRMI see alog 44348
thresh = {'SRXY':                {'ON':0.3,  'OFF':0.25}, # JCD, CRC 2Nov2018
          'ARM_IR':              {'ON':0.2,  'OFF':0.05},
          'ARM_IR_FMs':          {'ON':0.25, 'OFF':0.2},
          'DRMI_MICH_noarms':    {'ON':20,   'OFF':5},
          'DRMI_MICH_als':       {'ON':37,   'OFF':18},
          'DRMI_MICH_FMs':       {'ON':37,   'OFF':18},
          'DRMI_MICH_FMs_noarms':{'ON':24,   'OFF':5}, #added TVo DDB GLM 20181125
          'PRMIsb_MICH':         {'ON':8,   'OFF':5}, # Old Values 8 and 5. Lowered for low power PRMI locking, CRC July 23, 2018
          'PRMIcar_MICH':        {'ON':100,  'OFF':5},
          'DRMI_PRCL':           {'ON':-100, 'OFF':-100},
          'DRMI_PRCL_FMs':       {'ON':3.5,  'OFF':1},
          'DRMI_SRCL_noarms':    {'ON':10,   'OFF':5}, # Jul 26 2018, was 10, 5, Aug 5 2018, was 1, 2,
          'DRMI_SRCL_noarms_FMs':{'ON':5.5,  'OFF':1},
          'DRMI_SRCL_als':       {'ON':37,   'OFF':18},# 20180926: was 30,15 # Jul 26 2018, was 20, 10, Aug 5 2018, was 10, 1, Aug 22 2018, was 20, 15, 25
          'DRMI_SRCL_fromPRMI':  {'ON':48,  'OFF':5}, # 20180926: was 40,15 # Aug 22 2018, was 35, 15, 25
          'DRMI_SRCL_als_FMs':   {'ON':5.5,  'OFF':1}, #{'ON':40,  'OFF':18},#20181211 Dan+Rana # 20180926: was 40,15  # Aug 5 2018, was 5.5, 1, Aug 22 2018, was 20, 15, 25
          'DRMI_SRCL_noarms_FMs':{'ON':10,  'OFF':5}, # Aug 5 2018, was 5.5, 1
          'DRMI_ASC':            {'ON':0.001, 'CAUTION':0.001}, # Need to be updated for AS_C_NSUM
          'DARM':                {'ON': -100,  'OFF': -100},
          'ARMs_Green':          {'QUIET': 0.01, 'LOCKED': 0.8}, # LOCKED was 0.85; 08/19/2018
          'Override':            {'ON': -100, 'OFF': -100},
          'LOCKLOSS_CHECK':      {'ON': 601, 'OFF': 600},  # POP_A_DC should be above 900 by this time.   JCD 4Jan2019
          'MICHDARK':            {'LOCKED': 2000},
          'MICHBRIGHT':          {'LOCKED': 6000}, #CHANGED FROM 1000 GLM DDB 20181123
          'PRXY':                {'LOCKED': 1, 'OSCILLATING': 6},
         }

# trigger masks
trig_mask = {'MICH':[2],
             'PRCL':[2],
             'SRCL':[]}

trigDelay = {'PRCL_FM':0.2,
             'MICH_FM':0.1,
             'SRCL_FM':0.25}

#FMtrigDelaySRCL = 0.25 # was 0.25 before 20180922

lsc_mtrx_elems = {'MICH':4,
                  'PRCL':3.5, # was 3.5 (GV 2018-09-27),
                  'SRCL':4}

offset = {'SRCL_MODEHOP':-800}



# PD gain settings during acquisition
pd_gain = {
    'LSC': {
        'REFLAIR': {
            '9':  0.25,
            '45': 1,
        }, # close PD
        'POPAIR': {
            '9':  1,
            '18': 2,
            '45': 1,
            '90': 4,
        }, # close PD
        'REFL': {
            '9':  2,
            '45': 0.275,
        }, # close PD
        'POP': {
            '9':  8,
            '45': 1,
        }, # close PD
    }, # close LSC
    'ASC': {
        'REFL': {
            '9':  1,
            '45': 1,
        }, # close PD
        'POP': {
            'X': 2.8,
        }, # close PD
        'AS': {
            '36': 2.8,
            '45': 1,
        }, # close PD
    }, # close ASC
} # close full dict

# parameters related to CARM (offset reduction) sequence
carmHO_OFSref      =-40   # reference OFFSET for checking REFL_DSC
carmHO_PrefFraction= 0.57 # from alog 43346, REFL_DC fraction at OFFSET = -40 (arm transmission 800)
carmHO_OFSideal    =-50   # was -54 this is the target corresponding to about 85% of arm buildup for handoff to RF.

power_up_on_RF = False
use_reflair = False
reflair_relative_gain = 23  # we need 23dB more alanog gain if we are using REFL air than using REFL (lho alog 44378), and the same polarity

# AS Camera nominal exposure
camera_exp = {'ASAIR': 10000}

# VIOLIN damping settings
# list of filter modules to turn on, and gain, and drive dof (pit=default)
vio_settings = {
    'ITMX': {
        '1': {
            'FMs':['FM1', 'FM3', 'FM4'],
            'gain': 1,
            'drive': 'Y',
            'max_gain': 2, # didn't ring up but didn't damp after 20 minutes at 4 on Y
        }, #close mode
        '2': {
            'FMs':  ['FM1','FM2','FM4'],
            'gain': -5,
            'drive': ['P'],
            'max_gain': -5,
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM3', 'FM4'],
            'gain': 5,
            'drive': ['P'],
            'max_gain': 5,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM2','FM4'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
        }, # close mode
        '5': {
            'FMs':  ['FM1', 'FM2', 'FM4'],
            'gain': -5,
            'drive': ['P'],
            'max_gain': -5,
        }, # close mode
        '7': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 10,
            'drive': ['P'],
            'max_gain': 10,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM2','FM4'],
            'gain': 10,
            'drive': ['P'],
            'max_gain': 10,
        }, # close mode
        '9': {
            'FMs':  ['FM1','FM2','FM4'],
            'gain': -0.40, #was 0.4, rung up mode 1 GLM SED 20181130 (ITM RH change?)
            'drive': ['Y'],
            'max_gain': -0.4,
        }, # close mode


#        '11': { # 995.19
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 800,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '12': { # 995.47
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 300,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '13': { # 998.09
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 100,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '14': { # 1001.84
#            'FMs': ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
#            'gain': 1000,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '15': { # 1001.94
#            'FMs': ['FM1', 'FM2', 'FM6', 'FM10'], # 150 deg
#            'gain': 800,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '16': { # 1002.84
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '17': { # 1003.12
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
    }, # close ITMX

    'ITMY': {
        '1': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -50,
            'drive': ['P'],
            'max_gain': -50,
        }, # close mode
        '2': {
            'FMs':  ['FM1','FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM3', 'FM4'],
            'gain': 2,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM3', 'FM4'], #this is the tricky doublet
            'gain': -3,
            'drive': ['P'],
            'max_gain': -3,
        }, # close mode
        '5': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -20,
            'drive': ['P'],
            'max_gain': -20,
        }, # close mode
        '6': {
            'FMs':  ['FM1','FM3','FM4'],
            'gain': 2,
            'drive': ['Y'],
            'max_gain': 2,
        }, # close mode
        '7': {
            'FMs':  ['FM1','FM2','FM4'],
            'gain': 0.1,
            'drive': ['Y'],
            'max_gain': 0.2,
        }, # close mode
        '8': {
            'FMs':  ['FM1','FM4'],#before pre-loading this was FM1,3,4, negaitve gain, after no phase, +gain
            'gain': 4,
            'drive': ['Y'],
            'max_gain': 4,
        }, # close mode
        '9': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -5,
            'drive': ['P'],
            'max_gain': -5,
        }, # close mode


#        '11': { # 991.63
#            'FMs': ['FM1', 'FM10'], # 0 deg
#            'gain': 500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '12': { # 991.81
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 1500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '13': { # 994.81
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '14': { # 995.06
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 800,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '15': { # 997.63
#            'FMs': ['FM1', 'FM5', 'FM10'], # 60 deg
#            'gain': 500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '16': { # 997.78
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 200,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '17': { # 998.97
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 80,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
    }, # close ITMY

    'ETMX': {
        '2': {
            'FMs':  ['FM1','FM2','FM4'],
            'gain': -0.2,
            'drive': ['Y'],
            'max_gain': -20,
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM3','FM4'],
            'gain': 15,
            'drive': ['P'],
            'max_gain': 15,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 20,
            'drive': ['P'],
            'max_gain': 20,
        }, # close mode
        '6': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
        }, # close mode
        '7': {
            'FMs':  ['FM1','FM2', 'FM4'],
            'gain': 10,
            'drive': ['Y'],
            'max_gain': 10,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM2', 'FM4'],
            'gain': -10,
            'drive': ['P'],
            'max_gain': -10,
        }, # close mode
        '9': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -10,
            'drive': ['Y'],
            'max_gain': -10,
        }, # close mode


#        '11': { # 1006.44
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 40,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '12': { # 1006.75
#            'FMs': ['FM1', 'FM10'], # 0 deg
#            'gain': 160,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '13': { # 1010.34
#            'FMs': ['FM1', 'FM10'], # 0 deg
#            'gain': 30,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '14': { # 1010.59
#            'FMs': ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
#            'gain': 500,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '15': { # 1011.06
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 25,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '16': { # 1013.88
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 30,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '17': { # 1014.09
#            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
#            'gain': 300,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
    }, # close ETMX

    'ETMY': {
        '1': {
            'FMs':  ['FM1','FM3', 'FM4'],  #SED changed the filter because I measured the frequency to be 510.71515 not 510.714 Oct 15 2018  #RXA changed phase to +60 from -60 Dec 13th 2018
            'gain': 0.1,
            'drive': ['Y'],
            'max_gain': 0.1,# gain of 2 rang it up damping on yaw
        }, # close mode
        '2': {
            'FMs':  ['FM1','FM3','FM4'],
            'gain': -2,
            'drive': ['Y'],
            'max_gain': -2,
        }, # close mode
        '3': {
            'FMs':  ['FM1','FM3', 'FM4'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -3.0,
            'drive': ['Y'],
            'max_gain': -3,
        }, # close mode
        '5': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 0.2,
            'drive': ['Y'],
            'max_gain': 2,
        }, # close mode
        '6': {
            'FMs':  ['FM1', 'FM3','FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': -4,
        }, # close mode
        '7': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -2,
            'drive': ['Y'],
            'max_gain': -2,
        }, # close mode
        '9': {
            'FMs':  ['FM2', 'FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '10': {
            'FMs':  ['FM2', 'FM4', 'FM5'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode




#        '11': { # 1000.06
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 200,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '12': { # 1000.44
#            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
#            'gain': 100,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '13': { # 1009.94
#            'FMs': ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
#            'gain': 100,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '14': { # 1017.97
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 100,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
#        '15': { # 1018.22
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 2000,
#            'drive': ['P'],
#            'max_gain': 0,
#        }, # close mode
    }, # close ITMY
} # close dict

vio_mon = {  #monitor levels for which the guardian should do certain things
    'RF_noise_floor':3.5, # this is the noise floor when
    'DC_noise_floor':3.5,
    'Rung_up':7.5,# this mode is rung up so higfh we should engage with low gain
    'Nominal':5}
